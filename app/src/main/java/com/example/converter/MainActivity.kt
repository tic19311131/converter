package com.example.converter

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.converter.databinding.ActivityMainBinding

class MainActivity : Activity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.button.setOnClickListener ({
            converter()
        })
        binding.result.setOnClickListener({
            binding.button.visibility=View.VISIBLE
        })
    }

    //val a : Float = valor.toString().toFloat()
    //val b : Float = 1.6.toFloat()

    fun converter(){

        val valor = binding.editTextNumber.text
        val a : Float = valor.toString().toFloat()
        val b : Float = 1.6.toFloat()
        //logica de conversion
        //var total : Float
        //total = (valor.toString().toFloat()*(1.6)
        //logica de mostrar resultado


        //

        binding.button.visibility = View.GONE
        binding.result.text=mult(a,b).toString()
        Toast.makeText(this," Total KM: " + binding.result.text, Toast.LENGTH_SHORT).show()
        //binding.result.text=valor
    }


    fun mult( a: Float, b: Float): Float {
        return a * b
    }
}